# Coding test

### Installation
1. clone repository `git clone git@bitbucket.org:mbulembaev/coding-test.git`
2. create virtualenv with python3.7 `virtualenv -ppython3 venv`
3. install requirements `pip install -r requirements/base.txt`
4. copy .env settings `cp project/settings/.env_sample project/settings/.env`

By default Coding test using SQLite

Add db settings to `.env`

`DATABASE_URL=sqlite:///sqlite.db`