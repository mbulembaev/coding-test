from .settings import *

CURRENT_INSTANCE = env('CURRENT_INSTANCE', default='dev')

if CURRENT_INSTANCE == 'prod':
    from .prod import *
elif CURRENT_INSTANCE == 'dev':
    from .dev import *
else:
    from .dev import *
