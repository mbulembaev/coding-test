from django.contrib import admin
from webapp import models


@admin.register(models.Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['id', 'get_first_name', 'get_last_name', 'dob']
    search_fields = ['user__first_name', 'user__last_name']

    def get_first_name(self, obj):
        return obj.user.first_name

    get_first_name.admin_order_field = 'first_name'
    get_first_name.short_description = 'First name'

    def get_last_name(self, obj):
        return obj.user.last_name

    get_last_name.admin_order_field = 'last_name'
    get_last_name.short_description = 'Last name'


@admin.register(models.Policy)
class PolicyAdmin(admin.ModelAdmin):
    list_display = ['customer', 'type', 'premium', 'cover']
    list_filter = ['customer', 'type']
