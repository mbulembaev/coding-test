from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .serializers import CustomerCreateSerializer, PolicyCreateSerializer


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def create_customer(request):
    serializer = CustomerCreateSerializer(data=request.data)
    if serializer.is_valid():
        user = request.user
        user.first_name = serializer.validated_data.pop('first_name')
        user.last_name = serializer.validated_data.pop('last_name')
        user.save()
        serializer.save(user=user)
        return Response({"message": "Customer created", "data": {"first_name": user.first_name,
                                                                 "last_name": user.last_name,
                                                                 "dob": serializer.instance.dob}})
    else:
        data = {
          "error": True,
          "errors": serializer.errors,
        }
        return Response(data)


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def create_policy(request):
    serializer = PolicyCreateSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save(customer=request.user.customer)
        return Response({"message": "Policy created", "data": serializer.data})
    else:
        data = {
          "error": True,
          "errors": serializer.errors,
        }
        return Response(data)
