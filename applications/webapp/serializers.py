from rest_framework import serializers
from webapp.models import Customer, Policy


class CustomerCreateSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(max_length=80)
    last_name = serializers.CharField(max_length=80)

    class Meta:
        model = Customer
        fields = ("first_name", "last_name", "dob")


class PolicyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Policy
        fields = ("type", "premium", "cover")
