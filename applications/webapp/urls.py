from django.conf.urls import url
from . import views

app_name = 'webapp'

urlpatterns = [
    url(r'^create_customer/$', views.create_customer, name="create_customer"),
    url(r'^create_policy/$', views.create_policy, name="create_policy"),
]
