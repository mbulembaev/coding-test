from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Customer(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='customer')
    dob = models.DateField(verbose_name=_('Date of birth'))

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

    class Meta:
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')


class Policy(models.Model):
    customer = models.ForeignKey('webapp.Customer', on_delete=models.CASCADE, related_name='policies')
    type = models.CharField(verbose_name=_('Type'), max_length=100)
    premium = models.IntegerField(verbose_name=_('Premium'))
    cover = models.IntegerField(verbose_name=_('Cover'))

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = _('Policy')
        verbose_name_plural = _('Policies')
